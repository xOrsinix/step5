class LazyProperty(val initializer: () -> Int) {
    var count=0
    var tmp=0
    val lazy: Int
        get() {
            if (count==0){
                count++
                tmp=initializer()
                return tmp
            }
            return tmp
        }
}